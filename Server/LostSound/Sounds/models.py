from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class SoundFile(models.Model):
    """
    data class for sound files
    """
    file_name = models.CharField(max_length=500)  # file name
    file_type = models.CharField(max_length=20)  # file type
    created_at = models.DateTimeField(auto_now_add=True)  # file size
    file = models.FileField(upload_to='static/uploads/')  # the file itself
    shared = models.BooleanField(default=False)  # is shared to public
    owner = models.ForeignKey(User, on_delete=models.CASCADE, default=User.objects.get(id=1))  # the user that created the file

    def __str__(self):
        """
        short cut to return the file name
        :return: file name as string
        """
        return self.file_name

    def __int__(self):
        """
        short cut to return the file size
        :return: file size as int
        """
        return self.size
