from django import forms


class SoundFileForms(forms.Form):
    """
    form type class to upload a file
    """
    file = forms.FileField(widget=forms.FileInput(attrs={"accept": ".wav,.ogg,.mp3", "onchange": "form.submit()", "id": "file-input", "style": "display: none;"}))  # the newly uploaded file


class EditNotesForm(forms.Form):
    """
    form type class to edit data
    """
    def __init__(self,*args,**kwargs):
        self.name = kwargs.pop('name')
        self.shared = kwargs.pop('status')
        self.tempo = kwargs.pop('tempo')
        super(EditNotesForm,self).__init__(*args,**kwargs)
        self.fields['title'].widget = forms.TextInput(attrs={"id": "title-input", "name": "title-input", "class": "form-control", "value": self.name, "placeholder": "Enter new name for notes"})
        self.fields['shared'].widget = forms.CheckboxInput(attrs={"id": "shared-input", "name": "shared-input","checked": self.shared})
        self.fields['tempo'].widget = forms.NumberInput(attrs={"id": "tempo-input", "name": "tempo-input", "class": "form-control", "value": self.tempo, "placeholder": "Enter new tempo for notes"})

    title = forms.CharField(required=False, min_length=1, max_length=50)  # the title
    shared = forms.BooleanField(required=False)  # share with other people
    tempo = forms.IntegerField(required=True, min_value=1)
