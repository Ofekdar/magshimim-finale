from django.shortcuts import render, redirect, HttpResponse
from Sounds.forms import SoundFileForms, EditNotesForm
from Sounds.models import SoundFile
import Engine.Engine as engine
import os
import json
import mutagen
from mutagen.id3 import TPE1, TIT2
from pydub import AudioSegment
AudioSegment.converter = r"Engine/ffmpeg/ffmpeg.exe"

SUPPORTED_FORMATS = ("wav", "mp3", "ogg")
MAX_FILE_UPLOAD_SIZE = 50 * 1024 * 1024

TEMPO_ROW = 2
INSTRUMENT_ROW = 3


def upload(request):
    """
    the page where the user can upload a sound file or go record with the microphone
    :param request: the request with the information
    :return: the notes if user uploaded file. if not, the recording options page with the request and file uploading form
    """

    if request.user.is_authenticated:  # check if user is logged in, if not send him to login page
        if request.method == 'POST':  # check if it is a post type request, if not load the regular page so user can post the request

            soundFile = SoundFile()  # create a sound file type
            form = SoundFileForms(request.POST, request.FILES)  # get the user input

            if form.is_valid():  # make sure the user input is valid

                file = request.FILES['file']

                # checking to make sure the file isn't too big
                if file.size > MAX_FILE_UPLOAD_SIZE:
                    form = SoundFileForms()
                    return render(request, 'record.html', {'form': form, 'error': 'File sent is too big! Maximum size is 10mb'})

                # checking if the file's type is supported
                if file.name[file.name.rfind('.') + 1:] not in SUPPORTED_FORMATS:
                    form = SoundFileForms()
                    return render(request, 'record.html', {'form': form, 'error': 'File type not supported!'})

                # making a new instance of the SoundFile model for db
                soundFile.file = file  # get the file
                soundFile.size = len(soundFile.file)  # get the size
                soundFile.file_name = file.name[:file.name.rfind('.')]  # get the file name
                soundFile.file_type = file.name[file.name.rfind('.'):]  # get the file type
                soundFile.shared = True if request.POST.get("shared") == 'y' else False  # get if shared
                soundFile.owner = request.user  # get the owner of the file
                soundFile.save()  # save the file to the database
                AudioSegment.from_file(soundFile.file.path).export(out_f='static/uploads/' + str(soundFile.pk) + '.' + soundFile.file_name + '.wav' , format='wav')  # re-save as wav file with unique name
                os.remove(soundFile.file.path)  # get rid of the old file

                '''
                this code gives an exception when using small files because reading the file and not the path
                when reading the path, if the exception is caught, the os.remove throws another because AudioSegment still holds the file
                TODO: read from file properly or  disconnect AudioSegment from the file so os.remove works or keep file in uploads
                try:
                    AudioSegment.from_file(file).export(out_f='static/uploads/' + str(soundFile.pk) + '.' + soundFile.file_name + '.wav' , format='wav')  # re-save as wav file with unique name
                except IndexError:
                    os.remove(soundFile.file.path)
                    soundFile.delete()  # deleting the created docs
                    form = SoundFileForms()
                    return render(request, 'record.html', {'form': form, 'error': 'Could not open file properly, it could be corrupted or not made properly'})
                '''

                audio = mutagen.File('static/uploads/' + str(soundFile.pk) + '.' + soundFile.file_name + '.wav')  # add metadata to the wav file
                if isinstance(audio.tags, type(None)):  # add metadata tags if it doesnt exist
                    audio.add_tags()
                if "audio/vorbis" in audio.mime:
                    audio.tags['tit2'] = soundFile.file_name
                    audio.tags['tpe1'] = request.user.username
                else:
                    audio.tags['title'] = TIT2(encoding=3, text=soundFile.file_name)
                    audio.tags['artist'] = TPE1(encoding=3, text=request.user.username)
                audio.save('static/uploads/' + str(soundFile.pk) + '.' + soundFile.file_name + '.wav')  # save the metadata

                return redirect("/process/{}/".format(soundFile.pk))
        else:
            form = SoundFileForms()
        return render(request, 'record.html', {'form': form})
    return redirect("/login/")


def process(request, id):
    """
    send the file to the engine
    :param request: the request with the information
    :param id: the id of the file
    :return: the page with loaded notes
    """

    try:
        soundFile = SoundFile.objects.get(pk=id)
    except BaseException:
        return page_not_found(request)  # if doesnt exist return that couldnt find page

    try:
        text = engine.get_notes_string('static/uploads/' + str(id) + '.' + soundFile.file_name + '.wav')  # send file to engine
    except FileNotFoundError:
        return page_not_found(request)  # if file not found return error
    except Exception as e:
        form = SoundFileForms()
        return render(request, 'record.html', {'form': form, 'error': e})

    f = open("static\\saved_notes\\" + str(id) + ".txt","w+")  # create/open a file
    f.write(text)  # write the data from the engine in the file
    f.close()  # save the file

    return redirect("/notes/{}/".format(id))


def show_notes(request, id):
    """
    the page that shows the notes with alphatab to the user
    :param request: the request with the information
    :param id: 
    :return: the render of the notes display page with the request and the context (id of notes)
    """

    try:
        soundFile = SoundFile.objects.get(pk=id)  # get the file type by the id
    except BaseException:
        return page_not_found(request)

    if os.path.exists('static/uploads/' + str(id) + '.' + soundFile.file_name + '.wav') and (soundFile.shared or request.user.is_authenticated and request.user == soundFile.owner):
        return render(request, 'notes.html',context={ "id": id})  # if file exists show the notes with alphatab
    else:
        return page_not_found(request)


def page_not_found(request):
    """
    send a page not found error
    :param request: the request with the information
    :return: the page not found error
    """

    return render(request, 'page_not_found.html', status=404)


def record_mic(request):
    """
    the mic recording page where the user is able to record his microphone and send to the server
    :param request: the request with the information
    :return: the notes if user uploaded recording. if not, the mic recording page with the request
    """

    if request.user.is_authenticated:  # check if user is logged in, if not send him to the login page
        if request.method == 'POST':  # check if the request type is post, if not let him do one

            soundFile = SoundFile()  # create a sound file type
            form = SoundFileForms(request.POST, request.FILES)  # get the post data from user

            if form.is_valid():  # check is the data is valid

                # making a new instance of the SoundFile model for db
                soundFile.file = request.FILES['file']  # set the file data
                soundFile.file.name = request.POST.get("file_name")  # set the file name
                soundFile.size = len(soundFile.file)  #  set the file size
                soundFile.file_name = request.POST.get("file_name")[:soundFile.file.name.rfind('.')]  # set the title
                soundFile.file_type = soundFile.file_name[soundFile.file_name.rfind('.'):]  # set the file type
                soundFile.shared = True if request.POST.get("shared") == 'y' else False  # set if shared to public or not
                soundFile.owner = request.user  # set the file's user
                soundFile.save()  # save the file

                AudioSegment.from_file(soundFile.file.path).export(out_f='static/uploads/' + str(soundFile.pk) + '.' + soundFile.file_name + '.wav' , format='wav')  # save the file as wav
                os.remove(soundFile.file.path)

                audio = mutagen.File('static/uploads/' + str(soundFile.pk) + '.' + soundFile.file_name + '.wav')
                if isinstance(audio.tags, type(None)):  # add the tags to file if doesnt exist
                    audio.add_tags()
                audio.tags['title'] = TIT2(encoding=3, text=soundFile.file_name)
                audio.tags['artist'] = TPE1(encoding=3, text=request.user.username)
                audio.save('static/uploads/' + str(soundFile.pk) + '.' + soundFile.file_name + '.wav')  # save the metadata

                response = {'id': soundFile.pk}
                return HttpResponse(json.dumps(response), content_type='application/json')  # return the data to the client to know where to refer to
        return render(request, 'record_mic.html')
    return redirect("/login/")


def edit_notes(request, id):
    """
    edit the file data
    :param request:
    :param id:
    :return:
    """

    try:
        soundFile = SoundFile.objects.get(pk=id)  # make sure the file exists
    except BaseException:
        return page_not_found(request)

    if os.path.exists('static/uploads/' + str(id) + '.' + soundFile.file_name + '.wav') and request.user.is_authenticated and request.user == soundFile.owner:

        tempo = 0
        with open('static/saved_notes/' + str(id) + '.txt', mode='r') as file: # getting the tempo from the file
            data = file.read()
            tempo = data.split('\\tempo ')[1].split('\n')[0]

        if request.method == 'POST':  # check if its a post type

            if request.POST.get("delete"):

                os.remove('static/uploads/' + str(id) + '.' + soundFile.file_name + '.wav')
                os.remove('static/saved_notes/' + str(id) + '.txt')
                soundFile.delete()

                return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")

            else:
                data = ''
                if (request.POST.get("title") and request.POST.get("title") != soundFile.file_name) or request.POST.get("tempo"):
                    with open('static/saved_notes/' + str(id) + '.txt', mode='r') as file: # reading all the file
                        data = file.read()

                if request.POST.get("title") and request.POST.get("title") != soundFile.file_name:  # check if user wants to rename the file

                    data = '\\title "' + request.POST.get("title") + '"' + data[data.find('\n'):] # changing the title

                    os.rename('static/uploads/' + str(id) + '.' + soundFile.file_name + '.wav','static/uploads/' + str(id) + '.' + request.POST.get("title") + '.wav')  #rename the file
                    soundFile.file_name = request.POST.get("title") # rename the file

                soundFile.shared = True if request.POST.get("shared") else False  # change the share data

                if request.POST.get("tempo") and request.POST.get("tempo") != tempo:
                    rows = data.split('\n')
                    data = '\n'.join(rows[:TEMPO_ROW]) + '\n\\tempo ' + request.POST.get("tempo") + '\n' + '\n'.join(rows[INSTRUMENT_ROW:])
                    tempo = request.POST.get("tempo")

                if data != '':
                    with open('static/saved_notes/' + str(id) + '.txt', mode='w') as file: # saving back into the file
                        file.write(data)

                soundFile.save()  # save the changes

        form = EditNotesForm(status=soundFile.shared, name=soundFile.file_name, tempo=tempo)  # if failed create a form type
        return render(request, 'edit_notes.html', context={"form": form, "soundFile": soundFile, "tempo": tempo})

    else:
        return page_not_found(request)
