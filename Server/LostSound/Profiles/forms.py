from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.core.exceptions import ValidationError
MINIMUM_PASS_CHARS = 6

class AuthForm(AuthenticationForm):
    """
    A form type class created for user login
    """
    username = forms.CharField(label='', max_length=254, widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder': 'Username'})) #input string type for username
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'class' : 'form-control', 'placeholder': 'Password'})) #input string type for password

    error_messages = {
        'invalid_login': _("Username and Password entered do not match"),
        'inactive': _("This account is inactive."),
    } #possible error messages in case of it fails

    def confirm_login_allowed(self, user):
        """
        check if a user is active, if not throw an error
        :param user: the user to check if is active
        :return: the user in case he is active.
        """
        if not user.is_active:
            raise forms.ValidationError(
                _("This account is inactive."),
                code='inactive',
            )
        return user

    def clean_username(self):
        """
        get the username from the input as a string
        :return: the username as string
        """
        username = self.cleaned_data['username'].lower()
        return username


class RegistrationForm(forms.Form):
    """
    A form type class created for user creation
    """
    username = forms.CharField(label='', min_length=6, max_length=150, widget=forms.TextInput(attrs={'class' : 'username form-control', 'placeholder': 'Username'})) #the new user's username
    email = forms.EmailField(label='', widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder': 'Email'}))#the email of the user
    password1 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'class' : 'form-control', 'placeholder': 'Password'})) #the new user's password
    password2 = forms.CharField(label='', widget=forms.PasswordInput(attrs={'class' : 'form-control', 'placeholder': 'Retype Password'})) #make sure the user entered the password correctly

    def clean_username(self):
        """
        get the username and check if it doesnt exists.
        :return: the username as string if it doesnt exist
        """
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("Username already exists")
        return username

    def clean_email(self):
        """
        get the email and check if it doesnt exist
        :return: the email as string if it doesnt exist
        """
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("Email already exists")
        return email

    def clean_password2(self):
        """
        get both password inputs and very they are equal
        :return: the password as string if both password inputs are equal
        """
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")

        return password2

    def save(self, commit=True):
        """
        save the user in database
        :param commit:
        :return: the newly saved user
        """
        user = User.objects.create_user(
            self.cleaned_data['username'],
            self.cleaned_data['email'],
            self.cleaned_data['password1']
        )
        return user
