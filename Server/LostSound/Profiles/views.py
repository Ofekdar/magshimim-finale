from django.shortcuts import render
from Profiles.forms import AuthForm
from Profiles.forms import RegistrationForm
from django.http import HttpResponseRedirect
import django.contrib.auth as auth
from django.contrib.auth.models import User
from Sounds.models import SoundFile
import os

def get_notes(request, mode):
    """
    get a list of viewable notes
    :param request: the client request message
    :param mode: if it's user mode or general mode
    :return: the note list
    """
    files = SoundFile.objects.filter(owner=request.user) if mode == 'user' else SoundFile.objects.filter(shared=True)
    notes = []
    for file in files:
        if os.path.isfile('static/uploads/' + str(file.pk) + '.' + file.file_name + '.wav'):
            notes += [{'title': file.file_name,'id': str(file.pk), 'date': file.created_at.strftime("%B  %e, %Y, %I:%M %p UTC"), 'shared': str(file.shared)}] if mode == 'user' else [{'title': file.file_name,'id': str(file.pk), 'owner': file.owner, 'date': file.created_at.strftime("%B  %e, %Y, %I:%M %p UTC")}]
    return notes


def home(request):
    """
    the home page that the user first sees when entering the site.
    :param request: the request with the information
    :return: the home page with the reply to the client
    """
    return render(request, "home.html")


def user_notes(request):
    """
    the page where a user can see all notes he recorded and saved
    :param request: the request with the information
    :return: the notes page with the request and the notes from the db as context
    """
    if request.user.is_authenticated:
        context = {'notes': get_notes(request, 'user'), 'mode': 'user'}
        return render(request, "user_notes.html", context=context)
    else:
        return HttpResponseRedirect("/login/")


def public_notes(request):
    """
    the page where a user can see all the public notes user published
    :param request: the request with the information
    :return: the notes page with the request and the notes from the db as context
    """
    context = {'notes': get_notes(request, 'public'), 'mode': 'public'}
    return render(request, "user_notes.html", context=context)


def register(request):
    """
    the register page for new users who want to sign up
    :param request: the request with the information
    :return: the register form page if not logged in, if logged in or newly logged in refer to sound page.
    """
    if not request.user.is_authenticated:
        if request.method == 'POST':
            form = RegistrationForm(request.POST)  # get the registration form class
            if form.is_valid():

                user = form.save()  # save the form
                auth.login(request, user)  # login to the newly created user
                if request.user.is_authenticated:
                    return HttpResponseRedirect("/record/")  # if successful senduser to the record page
        else:
            form = RegistrationForm()
        return render(request, 'login-register.html', {'form': form, 'mode': 'Register'})  # load the registration form page
    return HttpResponseRedirect("/record/")  # if user is already logged in send him to the record page


def login(request):
    """
    the login page for new users who signed up but are logged out
    :param request: the request with the information
    :return:  the login form page if not logged in, if logged in or newly logged in refer to sound page.
    """
    if not request.user.is_authenticated:
        if request.method == 'POST':
            form = AuthForm(request,request.POST)
            if form.is_valid():
                user = User.objects.filter(username__icontains=form.clean_username())[0]
                auth.login(request, form.confirm_login_allowed(user))
                if request.user.is_authenticated:
                    return HttpResponseRedirect("/record/")  # user just logged in, therefore refer to record page

        else:
            form = AuthForm()  # create a new login form
        return render(request, 'login-register.html', {'form': form, 'mode': 'Login'})  # return the page with the login form
    return HttpResponseRedirect("/record/")  # user already logged in, therefore refer to record page


def logout(request):
    """
    the logout path, disconnects the user and then sends him to the login page
    :param request: the request with the information
    :return: the login form page
    """
    if request.user.is_authenticated:
        auth.logout(request)
    return HttpResponseRedirect("/login/")

# API VERSIONS OF PAGES
# =============================================================================================


def api_user_notes(request):
    """
    get the user notes as raw data
    :param request: the request with the information
    :return: the data
    """
    files = SoundFile.objects.all()
    notes = {}
    i = 1

    for file in files:
        if os.path.isfile('static/uploads/' + str(file.pk) + '.' + file.file_name + '.wav'):
            notes[i] = {'title': file.file_name, 'id': file.pk, 'date': file.created_at.strftime("%B  %e, %Y, %I:%M %p UTC"), 'shared': str(file.shared)}
            i += 1

    return render(request, 'api.html', {'returned_data': notes})
