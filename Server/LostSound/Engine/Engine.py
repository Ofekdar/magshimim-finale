import Engine.SoundCleaner as SoundCleaner
import Engine.Analyzer as Analyzer
import Engine.Visualizer as Visualizer
import Engine.BpmDetector as BpmDetector
import mutagen

SUPPORTED_FORMATS = ("wav", "mp3", "ogg")
HARMONIC_MARGIN = 1


def get_notes_string(file_path):
	"""
	This function makes notes from the audio in the path sent
	The returned string is meant to be in a file for AlphaTab to read
	:param file_path: the path of the audio to turn to notes
	:return: the string of the audio notes' containing the title, artist, tempo, tuning, instrument and the notes
	"""

	file_type = file_path[file_path.rfind('.') + len('.'):]						# getting the sound's format (even if file name says it's different)
	if file_type not in SUPPORTED_FORMATS:
		raise ImportError("Unknown file type")

	harmonic, sr = SoundCleaner.load_harmonic(file_path)						# get harmonic sound only and the sample rate

	frequencies_list = SoundCleaner.perform_fft(harmonic, sr)					# get a list of lists with all frequencies

	notes = Analyzer.find_lengths(Analyzer.find_peaks(frequencies_list))		# turn the frequencies into notes

	data = mutagen.File(file_path)												# get the metadata of the file

	tempo = BpmDetector.get_audio_bpm(file_path)								# get the tempo of the audio

	return Visualizer.visualize_notes(notes, dict(data), tempo)					# return the visualized notes in AlphaTex format
