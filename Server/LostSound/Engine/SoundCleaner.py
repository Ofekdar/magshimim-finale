import librosa
import numpy as np
from pydub import AudioSegment
AudioSegment.converter = r"Engine/ffmpeg/ffmpeg.exe"

HARMONIC_MARGIN = 1
MIM_DECIBELS = -50
CHUNK_SIZE = 10
SEGMENT_DIVIDE = 4
SAMPLE_POINTS = 8400
SECONDS_TO_MILLISECONDS = 1000
HALF = 2
DIVIDED_FROM_SAMPLES = 2.0


def detect_leading_silence(audio):
	"""
	this function gives the time where the sound from the start stops being quiet
	:param audio: the audio to check
	:return: the time where the audio stops being silent from the start
	"""

	trim_ms = 0

	assert CHUNK_SIZE > 0  # to avoid infinite loop
	while audio[trim_ms:trim_ms + CHUNK_SIZE].dBFS < MIM_DECIBELS and trim_ms < len(audio):
		trim_ms += CHUNK_SIZE

	return trim_ms


def load_harmonic(file_path):
	"""
	this function loads and separates the audio sent to it's harmonic values only
	:param file_path: the path of the audio to separate
	:return: the harmonic part of the audio sent and the sample rate of the audio
	"""

	# get the offset of the first and last places where sound is "noisy"
	sound = AudioSegment.from_file(file=file_path)
	offset = detect_leading_silence(sound) / SECONDS_TO_MILLISECONDS
	end = (len(sound)-detect_leading_silence(sound.reverse())) / SECONDS_TO_MILLISECONDS

	if end < offset:
		raise Exception("Audio sent is too short!")

	# load the sound without the silent parts and return only the harmonic with the sample rates
	audio, sr = librosa.load(file_path, offset=offset, duration=(end-offset))
	return librosa.effects.harmonic(audio, margin=HARMONIC_MARGIN), sr


def perform_fft(y_harmonic, sample_rate):
	"""
	this function performs fourier transform on the harmonic audio
	:param y_harmonic: the harmonic part of the audio
	:param sample_rate: the amount of samples per seconds of the audio
	:return: a list containing lists of the fft results on the audio
	"""

	# making sure the audio has sound to analyze
	if len(y_harmonic) == 0:
		raise Exception("no audio for fft")
	
	tf = sample_rate // SEGMENT_DIVIDE
	transforms = []

	# going over each frame of time in the audio
	while tf < len(y_harmonic) + sample_rate // SEGMENT_DIVIDE - 1:

		if tf >= len(y_harmonic):
			tf = len(y_harmonic) - 1

		# if the current frame isn't silent, perform fft on it and add the result to the list
		if len(y_harmonic[tf - sample_rate // SEGMENT_DIVIDE: tf]) != 0:
			yf = np.fft.fft(y_harmonic[tf - sample_rate // SEGMENT_DIVIDE: tf])
			yf = DIVIDED_FROM_SAMPLES/SAMPLE_POINTS * np.abs(yf[:SAMPLE_POINTS//HALF])
			transforms.append(yf)

		tf += sample_rate // SEGMENT_DIVIDE
	
	return transforms	
