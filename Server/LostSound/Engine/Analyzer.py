import math
from detecta import detect_peaks

SEGMENT_DIVIDE = 4  # MUST BE SAME AS SEGMENT DIVIDE IN SoundCleaner.py
CHUNK_LENGTH = 1 / SEGMENT_DIVIDE
MIN_VALUE = 1 / 5
SAMPLE_POINTS = 8400  # MUST BE SAME AS SAMPLE_POINTS IN SoundCleaner.py

DEFAULT_NOTES = {27.5: 'A', 29.13524: 'A#', 30.86771: 'B', 32.70320: 'C', 34.64783: 'C#', 36.70810: 'D', 38.89087: 'D#', 41.20344: 'E', 43.65353: 'F', 46.24930: 'F#', 48.99943: 'G', 51.91309: 'G#'}
MIN_HERTZ = 27.5  # A in octave number 0
MAX_HERTZ = 4186.009  # C in 8th octave
MAX_OCTAVE = 8
MAX_NOTE = 11
OCTAVE_HIGHER = 2
HIGHER_LOWER_DIFFERENCE = 3
PEAKS_MPD = 20
PEAKS_MPH_MUL = 100


def find_lengths(peaks):
    """
    A function to add length to the notes
    :param peaks: the note's peaks
    :return: notes lengths in format: [[note, {"start": start_time, "end": end_time}], [...]] if silent note = "r"
    """

    # if there weren't any peaks in audio - cannot determine lengths
    if len(peaks) == 0:
        raise ImportError("no peaks in audio")

    # going over the peaks, checking the lengths of each one
    lengths = []
    for i in range(0, len(peaks)):

        # if the pike was empty, it is silent
        if len(peaks[i]) == 0:
            # if it was silent before, make it longer, else add another silent note to the dict
            if len(lengths) != 0 and lengths[-1][0] == "r":
                lengths[-1][1]["end"] += CHUNK_LENGTH
            else:
                lengths.append(["r", {"start": CHUNK_LENGTH * i, "end": CHUNK_LENGTH * (i + 1)}])

        # if the pike has 1 or more elements, make sure to enter all of them to the dict (there might be multiple notes at certain times)
        for pike in peaks[i]:
            note_value = hertz_to_note(pike)
            indexes = [j for j, note in enumerate(lengths) if note[0] == note_value]
            # if the note is repeated, make it longer, else add another note to the dict
            if len(indexes) != 0 and lengths[max(indexes)][1]["end"] == CHUNK_LENGTH * i:
                lengths[max(indexes)][1]["end"] += CHUNK_LENGTH
            else:
                lengths.append([note_value, {"start": CHUNK_LENGTH * i, "end": CHUNK_LENGTH * (i + 1)}])

    return lengths


def hertz_to_note(hertz_value):
    """
    this function finds the closes note to the hertz value given
    :param hertz_value: the value (in hertz) of the note to find
    :return: the note the hertz corresponds to
    """

    # checking the hertz value is in the range we allow
    hertz_value *= SEGMENT_DIVIDE
    if hertz_value < MIN_HERTZ or hertz_value > MAX_HERTZ:
        return 'r'  # hertz not in range

    # going over all 9 possible octaves on standard piano
    for i in range(0, MAX_OCTAVE):
        # going over all 12 notes (including half octaves)
        for j in range(1, MAX_NOTE):
            # checking if note between current and last note
            if hertz_value < list(DEFAULT_NOTES.keys())[j] * math.pow(OCTAVE_HIGHER, i):

                # getting either the note below or the one above, whichever is closest
                note_letter = DEFAULT_NOTES[min(list(DEFAULT_NOTES.keys()), key=lambda x:abs(x-(hertz_value / math.pow(OCTAVE_HIGHER, i))))]
                note_octave = str(i if j < HIGHER_LOWER_DIFFERENCE else i + 1) if note_letter != 'B' or j != 3 else str(i)

                return note_letter + note_octave

    return None  # will never happen!


def find_peaks(freq_list):
    """
    this function finds peaks in the array of frequencies sent
    :param freq_list: the array of frequencies per segment
    :return: a list of all peaks in each segment
    """

    peaks_freq_list = []
    for freq in freq_list:
        # get the sub frequencies for the current frequency and add them to the list of peaks
        sub_freq = []
        for i in detect_peaks(freq, mph=(PEAKS_MPH_MUL*sum(freq)/len(freq)), mpd=PEAKS_MPD, show=False):
            sub_freq.append(i)
        peaks_freq_list.append(sub_freq)

    return peaks_freq_list
