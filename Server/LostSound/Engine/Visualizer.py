MAX_TIME = 2
NOTES_POWER = 0.5
OPPOSITE_NOTES_POWER = 1 / NOTES_POWER


def next_break_recursion(filler, power=1):
    """
    a function to determine the next lowest break that can be added to the last bar
    :param filler: the amount of time to fill out
    :param power: the power of 0.5 to check for (in music terms - half note, then a quarter note and so on...)
    :return: the power of the next lowest note to add to the filler
    """
    return power if (filler / pow(NOTES_POWER, power)).is_integer() else next_break_recursion(filler, power=power + 1)

'''
A function to turn the dicts of notes into a string that defines the notes for AlphaTab to read
input:
    - the array of notes
    - the metadata of the audio
    - the tempo of the audio
output:
    - the string of text to write to a file for AlphaTab to read
'''
def visualize_notes(note_array, metadata, tempo):

    # Making the initial string with the title, artist, tempo, tuning and instrument of the audio
    final_string = "\\title \"" + (metadata['tit2'][0] if "tit2" in metadata else metadata['TIT2'][0] if "TIT2" in metadata else "Unknown")
    final_string += "\"\n\\artist \"" + (metadata['tpe1'][0] if "tpe1" in metadata else metadata['TPE1'][0] if "TPE1" in metadata else "Unknown Artist")
    final_string += "\"\n\\tempo " + (str(tempo) if tempo != -1 else "200") + "\n\\instrument 1\n.\n  \staff{score}\n\\tuning piano\n\\instrument  acousticgrandpiano\n"

    bar = 1
    current_time = round(note_array[0][1]['start'], 2)
    end = max([round(sublist[1]['end'], 2) for sublist in note_array])

    print(note_array)

    # Adding the notes to the string (in the format for AlphaTab)
    while end > current_time:

        wanted_notes = []
        dur = 9999999999999

        # get all notes within current time
        for note in note_array:
            if round(note[1]['start'], 2) <= current_time and round(note[1]['end'], 2) > current_time:
                if note[0] != 'r':
                    wanted_notes.append(note[0])
                time = MAX_TIME//(round(note[1]['end'], 2) - current_time)
                if dur > time:
                    dur = time

        # adding the note to the final strings
        if dur < 1:
            dur = 1
        if current_time + MAX_TIME//dur > bar * MAX_TIME and current_time < bar * MAX_TIME:
            dur = MAX_TIME//(bar * MAX_TIME - current_time)
        current_time += MAX_TIME/dur
        final_string += 'r.' + str(int(dur)) + ' ' if len(wanted_notes) == 0 else '(' + " ".join(wanted_notes) + ').' + str(int(dur)) + ' '
        if current_time >= MAX_TIME * bar:
            final_string += '| '
            bar += 1

    #  adding additional breaks at end to fill the rest of the last bar
    if '| ' not in final_string:
        raise Exception("Audio sent/recorded is always silent")
    filler = sum([1 / int(note_len[0]) for note_len in final_string.split('| ')[-1].split('.')[1:]])
    while filler != 1:
        break_pow = next_break_recursion(filler)
        final_string += 'r. ' + str(pow(OPPOSITE_NOTES_POWER, break_pow)) + ' '
        filler += pow(NOTES_POWER, break_pow)

    return final_string



