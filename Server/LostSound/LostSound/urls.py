"""LostSound URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Sounds.views import *
from Profiles.views import *
urlpatterns = [

    path('admin/', admin.site.urls),
    path('record/', upload, name='record'),
    path('record/mic/', record_mic, name='record_mic'),
    path('', home),
    path('process/<int:id>/', process, name='process'),
    path('notes/<int:id>/', show_notes),
    path('profile/history/', user_notes, name='history'),
    path('public/notes/', public_notes),
    path('login/', login),
    path('logout/', logout),
    path('register/', register),
    path('notes/<int:id>/edit/', edit_notes),

    path('api/profile/history/', api_user_notes)
]
