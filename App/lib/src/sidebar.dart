import 'package:flutter/material.dart';

import 'pages/login.dart';
import 'pages/history.dart';
import 'pages/home.dart';

class SideBar extends StatefulWidget {
  @override
  _SideBarState createState() => _SideBarState();
}

class _SideBarState extends State<SideBar> {

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Center(
              child: Text(
                'Available Pages',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            decoration: BoxDecoration(
                color: Colors.green,
                /*
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('images/note.jpg')
                */
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Welcome'),
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage())),
          ),
          ListTile(
            leading: Icon(Icons.list),
            title: Text('Saved Notes'),
            onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HistoryPage())),
          ),
          Builder(
            builder: (context) => ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text('Logout'),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => LoginForm())),
              ),
          ),
        ],
      ),
    );
  }
}