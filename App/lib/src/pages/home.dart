import 'package:flutter/material.dart';

import '../sidebar.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: SideBar(),
      appBar: AppBar(
        title: Text('Lost & Sound'),
      ),
      body: Center(child: Text("Home Page")),
    );
  }
}
