import 'package:flutter/material.dart';

import '../sidebar.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: SideBar(),
        appBar: AppBar(
          title: Text('Lost & Sound'),
        ),
        body: ListView(
          children: [
            Form(
            key: _formKey,
            child: Column(
              children: [
                /* copyResize(Image.asset('images/note.png'), width: 120),*/
                /* Image(image: AssetImage('images/note.png')), */
                Padding(
                  padding: const EdgeInsets.only( top: 90, bottom: 30 ),
                  child: Text(
                    'Login To Your Account',
                    style: TextStyle(
                      fontSize: 26,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextFormField(
                    validator: (input) {
                      return input.isEmpty ? 'No Empty :/' : null;
                    },
                    decoration: InputDecoration(
                        hintText: 'Username',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextFormField(
                    validator: (input) {
                      return input.isEmpty ? 'No Empty :/' : null;
                    },
                    decoration: InputDecoration(
                      hintText: 'Password',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 50, ),
                  child: ElevatedButton(onPressed: () {
                      if(_formKey.currentState.validate()) {
                        Scaffold
                          .of(context)
                          .showSnackBar(SnackBar(content: Text('content yay :D')));
                      }
                    },
                    child: Text('wheewoo')
                  ),
                ),
              ],
            )
          )
        ]
      )
    );
  }
}
