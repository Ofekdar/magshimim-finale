import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';

import '../sidebar.dart';



class NotesInfo {
  final int id;
  final String title;
  final String date;
  final bool public;

  NotesInfo({this.id, this.title, this.date, this.public});

  factory NotesInfo.fromJson(Map<String, dynamic> json) {
    print("yay2");
    print(json);
    return NotesInfo(
      /*
      id: json['id'],
      title: json['title'],
      date: json['date'],
      public: json['shared'],
       */
      id: 5,
      title: "6",
      date: "7",
      public: true,
    );
  }
}

Future<NotesInfo> fetchNotes() async {
  print("yay4");
  final response = await http.get('http://192.168.1.181:8000/api/profile/history/');
  print("yay3");

  if (response.statusCode == 200) {
    return NotesInfo.fromJson(jsonDecode(response.body));
  }
  return null;
}

class HistoryPage extends StatefulWidget {
  @override
  _HistoryPageState createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {

  Future<NotesInfo> notesList;

  @override
  void initState() {
    super.initState();
    print("yay5");
    notesList = fetchNotes();
    print("yay6");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: SideBar(),
        appBar: AppBar(
          title: Text('Lost & Sound'),
        ),
        body: ListView(
          children: getUserNotes()
        ),
    );
  }

  List<Widget> getUserNotes() {

    List<Widget> list = [];

    list.add(Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Center(
        child: Text(
          'Saved Notes',
          style: TextStyle(
            fontSize: 27,
            color: Colors.blueGrey,
          ),
        ),
      ),
    ));

    list.add(FutureBuilder<NotesInfo>(
      future: notesList,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Text(snapshot.data.title);
        } else if (snapshot.hasError) {
          return Text("${snapshot.error}");
        }

        // By default, show a loading spinner.
        return CircularProgressIndicator();
      },
    ));

    for(var i = 0; i < 20; i++) {
      list.add(Center(
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(top: 20),
              child: OutlinedButton(
                onPressed: () {},
                child: makeButton(i.toString(), (i*i).toString(), i % 2 == 0 ? "yes" : "no"),
              ),
            ),
            width: 300,
      )));
    }

    return list;
  }

  Padding makeButton(String title, String createdAt, String public)
  {

    var _titleStyle = TextStyle(
      fontSize: 18,
      color: Colors.indigo,
    );
    var _detailsStyle = TextStyle(
      color: Colors.grey,
    );

    return Padding(
      padding: const EdgeInsets.only(bottom: 10, top: 10),
      child: Column(
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(title, style: _titleStyle),
          ),
          Divider(),
          Row(
            children: [
              Text("Created At: " + createdAt, style: _detailsStyle),
              Spacer(),
              Text("Status: " + public,style: _detailsStyle),
            ],
          ),
        ],
      ),
    );
  }
}