# Lost Sound <img src="Server/LostSound/static/images/icon.png" alt="drawing" width="40"/> Finale Project For Magshimim!

## Contents
 - [Project Description](#what-is-it?)
 - [Current Features](#what-can-it-do?)
 - [Project Architecture](#architecture)
 - [Requirements](#requirements)
 - [Roadmap](#roadmap)
 - [Contributors](#contributors)

## What is it?
A service that allows users to connect to a remote server and send audio of a piano playing music that will be processed in an engine that will try and figure the notes that were played. The notes then are returned back to the user and displyed for him just like normal sheet music!

In the main repository, you can find the server that you can run which will accept and send back requests. The server itself is equipped to respond to clients that send requests using a web API such as Chrome or Firefox. The second folder contains the files required in order to run an app on your phone using Flutter that will be able to connect to a running server and retrieve information required for things such as users or notes.

## What can it do?
**Server:**
- Run and allow clients to connect
- Accept and send web requests for specific url paths
- Send audio to engine to transform into notes
- Pretty and comfortable HTML pages for web users
- User mangagment system with authntication, log in or sign up options
- Database managment that holds all notes' details and users

**App:**
- Run on Android and connect to server IP
- Send and get requests from server
- display information using a uniqe app look

## Architecture
<img src="Server/LostSound/static/images/Architecture.png" alt="drawing"/>

## 	Requirements
**Server:**
* Python 3.X
* Install all dependencies from the `requirements.txt` file `pip install -r requirements.txt`
* Downloading ffmpeg for the file conversion

**Application:**
* Android Studio (or any other application that can run Flutter)
* Dart and Flutter installed
* The libraries from ``pubspec.yaml`` with ``flutter pub get``
* For running the app - An Android device or emulator

## Road Map

**Version 1.0**
* Initial Release
* A working site for piano instruments
* Account manager

**Version 1.1: The Application Update**
* Operating application to access from mobiles

**Version 1.2: The Engine Update**
* An enhanced engine file with better performance and better accuracy

**Version 1.3: The Instrument Update**
* Support for more instruments
* Open Source

## Contributors

* Sahar Ben David for Engine Logic, Connection between server side and client side, mobile application and uploaded to server.
* Ofek Daraby for Engine Logic, Coonection between server side and client side and Graphic Design.
* Matan Dobrushin for code support, helping in uploading to the server and mentor.
* Arad for being a guide of how to be organized within the documentary side of the project and giving us our mentor.
* The company Waves for contributing ideas of ways of how to continue with our project, such as improving the engine.

